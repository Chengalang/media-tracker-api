<?php

namespace App\Console\Commands;

use App\Imports\PublisherImport;
use App\Imports\VideoGamesImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Excel::import(new PublisherImport, database_path('games.csv'));
        Excel::import(new VideoGamesImport, database_path('games.csv'));
    }
}
