<?php

namespace App\Events;

use App\Models\VideoGame;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class VideoGameRetrieved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The video game instance.
     *
     * @var VideoGame
     */
    public $videoGame;
    public string $action;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(VideoGame $videoGame)
    {
        $this->videoGame = $videoGame;
        $this->action = 'retrieved';
    }
}
