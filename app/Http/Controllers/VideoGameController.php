<?php

namespace App\Http\Controllers;

use App\Http\Requests\IndexVideoGameRequest;
use App\Http\Requests\StoreVideoGameRequest;
use App\Http\Requests\UpdateVideoGameRequest;
use App\Http\Resources\VideoGameCollection;
use App\Http\Resources\VideoGameResource;
use App\Models\VideoGame;
use Illuminate\Http\JsonResponse;

class VideoGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexVideoGameRequest $request
     * @return VideoGameCollection
     */
    public function index(IndexVideoGameRequest $request): VideoGameCollection
    {
        $videoGame = new VideoGame;

        return new VideoGameCollection($videoGame->getFiltered($request->validated()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreVideoGameRequest $request
     * @return VideoGameResource
     */
    public function store(StoreVideoGameRequest $request): VideoGameResource
    {
        $game = VideoGame::create($request->all());

        return new VideoGameResource($game);
    }

    /**
     * Display the specified resource.
     *
     * @param VideoGame $videoGame
     * @return VideoGameResource
     */
    public function show(VideoGame $videoGame): VideoGameResource
    {
        $videoGame->load('publisher');

        return new VideoGameResource($videoGame);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateVideoGameRequest $request
     * @param VideoGame $videoGame
     * @return VideoGameResource
     */
    public function update(UpdateVideoGameRequest $request, VideoGame $videoGame): VideoGameResource
    {
        $videoGame->update($request->all());

        return new VideoGameResource($videoGame);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param VideoGame $videoGame
     * @return JsonResponse
     */
    public function destroy(VideoGame $videoGame): JsonResponse
    {
        return response()->json(['deleted' => $videoGame->delete()]);
    }
}
