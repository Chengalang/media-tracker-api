<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndexVideoGameRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|string',
            'year' => 'nullable|numeric',
            'est_dev_cost' => 'nullable|numeric',
            'est_marketing_cost' => 'nullable|numeric',
            'est_total_cost' => 'sometimes|numeric',
            'profile_url' => 'sometimes|url',
            'image_url' => 'sometimes|url',
            'description' => 'sometimes|',
            'publisher_id' => [
                'nullable',
                'numeric',
                !is_null(request()->publisher_id) ? 'exists:publishers,id' : ''
            ]
        ];
    }
}
