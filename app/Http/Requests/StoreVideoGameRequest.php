<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreVideoGameRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'year' => 'nullable|numeric|between:2022,2050',
            'est_dev_cost' => 'nullable|numeric',
            'est_marketing_cost' => 'nullable|numeric',
            'est_total_cost' => 'required|numeric',
            'profile_url' => 'required|url',
            'image_url' => 'required|url',
            'description' => 'required',
            'publisher_id' => 'nullable'
        ];
    }
}
