<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVideoGameRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes|required|string',
            'year' => 'nullable|numeric|between:2022,2050',
            'est_dev_cost' => 'nullable|numeric',
            'est_marketing_cost' => 'nullable|numeric',
            'est_total_cost' => 'sometimes|required|numeric',
            'profile_url' => 'sometimes|required|url',
            'image_url' => 'sometimes|required|url',
            'description' => 'sometimes|required|',
            'publisher_id' => [
                'nullable',
                'numeric',
                !is_null(request()->publisher_id) ? 'exists:publishers,id' : ''
            ]
        ];
    }
}
