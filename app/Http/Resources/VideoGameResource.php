<?php

namespace App\Http\Resources;

use App\Http\Resources\PublisherResource;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class VideoGameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'year' => $this->year,
            'est_dev_cost' => $this->est_dev_cost,
            'est_marketing_cost' => $this->est_marketing_cost,
            'est_total_cost' => $this->est_total_cost,
            'profile_url' => $this->profile_url,
            'image_url' => $this->image_url,
            'description' => $this->description,
            'publisher' => $this->when($this->publisher, function () {
                return new PublisherResource($this->publisher);
            })
//            'publisher' => new PublisherResource($this->whenLoaded('publisher'))
        ];
    }
}
