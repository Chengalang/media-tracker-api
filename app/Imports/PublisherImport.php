<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Publisher;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithUpserts;

class PublisherImport implements ToModel, WithUpserts, WithStartRow

{
    public function model(array $row): ?Publisher
    {
        if (empty($row[2])) {
            return null;
        }

        return new Publisher([
            'name' => $row[2]
        ]);
    }

    public function uniqueBy(): string
    {
        return 'name';
    }

    public function startRow(): int
    {
        return 2;
    }
}
