<?php

namespace App\Imports;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Publisher;
use App\Models\VideoGame;
use Maatwebsite\Excel\Concerns\WithStartRow;


class VideoGamesImport implements ToModel, WithStartRow
{
    public function model(array $row)
    {
        return new VideoGame([
            'name' => $row[0],
            'year' => $row[1] == 'cancelled' ? null : Carbon::createFromFormat('Y', $row[1])->year,
            'publisher_id' => Publisher::where('name', $row[2])->value('id'),
            'est_dev_cost' => $row[3],
            'est_marketing_cost' => $row[4],
            'est_total_cost' => $row[5],
            'profile_url' => $row[6],
            'image_url' => $row[7],
            'description' => $row[8]
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
