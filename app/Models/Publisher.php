<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\VideoGame;

class Publisher extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function games()
    {
        return $this->hasMany(VideoGame::class);
    }

    public function videoGame()
    {
        return $this->belongsTo(VideoGame::class);
    }
}
