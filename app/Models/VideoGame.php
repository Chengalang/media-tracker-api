<?php

namespace App\Models;

use App\Events\VideoGameCreated;
use App\Events\VideoGameDeleted;
use App\Events\VideoGameRetrieved;
use App\Events\VideoGameUpdated;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Publisher;
use Illuminate\Database\Eloquent\Relations\HasOne;

class VideoGame extends Model
{
    use HasFactory;

    protected $dispatchesEvents = [
        'created' => VideoGameCreated::class,
        'retrieved' => VideoGameRetrieved::class,
        'updated' => VideoGameUpdated::class,
        'deleted' => VideoGameDeleted::class
    ];

    protected $fillable = [
        'name',
        'year',
        'est_dev_cost',
        'est_marketing_cost',
        'est_total_cost',
        'profile_url',
        'image_url',
        'description',
        'publisher_id'
    ];

    protected function year(): Attribute
    {
        return Attribute::get(
            fn ($value) => is_null($value) || empty($value) ? 'cancelled' : $value
        );
    }

    public function publisher(): HasOne
    {
        return $this->hasOne(Publisher::class, 'id', 'publisher_id');
    }

    public function getFiltered(array $filters): Collection
    {
        return $this->filter($filters, 'name')
            ->filter($filters, 'year')
            ->filter($filters, 'est_dev_cost')
            ->filter($filters, 'est_marketing_cost')
            ->filter($filters, 'est_total_cost')
            ->filter($filters, 'profile_url')
            ->filter($filters, 'image_url')
            ->filter($filters, 'description')
            ->filterRelation($filters, 'publisher', 'publisher', isset($filters['publisher']) && is_numeric($filters['publisher']) ? 'id' : 'name')
            ->get();
    }

    public function scopeFilter($query, array $filters, string $key)
    {
        return $query->when(array_key_exists($key, $filters), fn ($q) => $q->where($key, $filters[$key]));
    }

    public function scopeFilterRelation($query, array $filters, string $key, string $relation, string $column)
    {
        return $query->when(array_key_exists($key, $filters), fn ($q) => $q->whereRelation($relation, $column, $filters[$key]));
    }

}
