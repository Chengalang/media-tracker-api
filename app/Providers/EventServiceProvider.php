<?php

namespace App\Providers;

use App\Events\VideoGameCreated;
use App\Events\VideoGameDeleted;
use App\Events\VideoGameRetrieved;
use App\Events\VideoGameUpdated;
use App\Listeners\LogVideoGame;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        VideoGameCreated::class => [
            LogVideoGame::class
        ],
        VideoGameRetrieved::class => [
            LogVideoGame::class
        ],
        VideoGameUpdated::class => [
            LogVideoGame::class
        ],
        VideoGameDeleted::class => [
            LogVideoGame::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
