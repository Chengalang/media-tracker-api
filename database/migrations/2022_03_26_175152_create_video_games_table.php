<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_games', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->year('year')->nullable();
            $table->string('est_dev_cost')->nullable();
            $table->string('est_marketing_cost')->nullable();
            $table->string('est_total_cost');
            $table->string('profile_url');
            $table->string('image_url');
            $table->text('description');

            // $table->string('publisher');
            $table->foreignId('publisher_id')->nullable()->constrained();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_games');
    }
};
