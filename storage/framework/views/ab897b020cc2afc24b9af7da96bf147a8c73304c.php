

<?php $__env->startSection('content'); ?>
    <div class="main-form">
        <form action="/api/video-games/search" method="POST">
            <?php echo csrf_field(); ?>

            <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                   id="search"
                   type="text"
                   name="search">

            <select id="search_type" name="search_type" class="shadow border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                <option value="name">Name</option>
                <option value="year">Year</option>
                <option value="publisher">Publisher</option>
            </select>

            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 ml-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                Search
            </button>
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('games.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\media-tracker\resources\views/games/list.blade.php ENDPATH**/ ?>