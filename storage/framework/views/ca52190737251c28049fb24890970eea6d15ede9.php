<?php $__env->startSection('content'); ?>
    <div class="main-form">
        <form action="/api/video-games/store" method="POST">
            <?php echo csrf_field(); ?>

            <div class="create">
                <div class="grid grid-cols-2 gap-1">
                    <label for="name">Name</label>
                    <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="name"
                           type="text"
                           name="name">
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="year">Years</label>
                    <select id="year" name="year" class="shadow border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        <option selected disabled></option>
                        <?php $__currentLoopData = $years; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $year): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($year); ?>"><?php echo e($year); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="est_dev_cost">Estimated Development Cost</label>
                    <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="est_dev_cost"
                           type="text"
                           name="est_dev_cost">
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="est_marketing_cost">Estimated Marketing Cost</label>
                    <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="est_marketing_cost"
                           type="text"
                           name="est_marketing_cost">
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="est_total_cost">Estimated Total Cost</label>
                    <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="est_total_cost"
                           type="text"
                           name="est_total_cost">
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="profile_url">Profile URL</label>
                    <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="profile_url"
                           type="url"
                           name="profile_url">
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="image_url">Image URL</label>
                    <input class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="image_url"
                           type="url"
                           name="image_url">
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="description">Description</label>
                    <textarea class="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                           id="description"
                           name="description">
                    </textarea>
                </div>

                <div class="grid grid-cols-2 gap-1">
                    <label for="publisher_id">Publishers</label>
                    <select id="publisher_id" name="publisher_id" class="shadow border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        <option selected disabled></option>
                        <?php $__currentLoopData = $publishers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $publisher): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($publisher->id); ?>"><?php echo e($publisher->name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                </div>
            </div>

            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mt-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                Submit
            </button>
        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('games.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\media-tracker\resources\views/games/create.blade.php ENDPATH**/ ?>